const testDiv = document.getElementsByClassName("edit");
const popUp = document.getElementById("pop-up");
const saveBtn = document.querySelector(".save");
const cancelBtn = document.querySelector(".cancel");
const popUpTitle = document.querySelector(".pop-up-title");
const inputEditField = document.getElementById("pop-up-input");
const headersOptionsUl = document.getElementsByClassName("header__options-ul");
const headersOptions = document.querySelectorAll(".header__options-ul li");
const sections = document.querySelectorAll(".section div");
const headerFollowers = document.querySelector(".header__followers")
const followersCaption = document.querySelector(".follower-caption")
const logoutBtn = document.querySelector(".header__logout-btn")
const popUpLog = document.querySelector(".pop-up-log")
let lastFieldText = [];

headerFollowers.addEventListener('mouseover', (e)=> {
    followersCaption.classList.remove('hide')
})

headerFollowers.addEventListener('mouseleave', (e)=> {
    followersCaption.classList.add('hide')
})

document.addEventListener('click', (event)=>{
    if (event.target.closest(".header__logout-btn")){
        popUpLog.classList.remove('hide')
    }else{
        popUpLog.classList.add("hide");
    }
})




/************************
TABS
************************/
function switchTabs(e) {
    const optionItemSelected = document.querySelector(`#${this.querySelector("a").getAttribute("id")}`)
    const sectionSelected = document.querySelector(`.${this.querySelector("a").getAttribute("id")}`)
    
    Object.keys(sections).map((elem)=>{
        sections[elem].classList.remove('active')
    })

    sectionSelected.classList.add('active')

    Object.keys(headersOptions).map((elem)=>{
        headersOptions[elem].querySelector("a").classList.remove('active')
    })

    optionItemSelected.classList.add('active')
}

/************************
SHOW POPUP - DESKTOP
************************/
function showPopUp() {
    (lastFieldText.length > 0 && (
        closePopUp()
    ))

    var fieldClass = this.parentNode.parentNode.getAttribute("class");
    var dataField = this.parentNode.parentNode.getAttribute("data-title");
    popUp.style.top = (this.offsetTop - 7 ) + "px";
    popUp.style.left = (this.offsetLeft + 15 ) + "px";
    popUp.classList.remove("hide");
    popUp.children[1].setAttribute("data-field", fieldClass);

    lastFieldText.push(this.previousElementSibling.querySelector('span').textContent);

    popUpTitle.textContent = dataField;

    inputEditField.focus();
    inputEditField.value = lastFieldText[0];
}

/************************
CANCEL - DESKTOP
************************/
function closePopUp() {
    var fieldElemeClass = cancelBtn.parentNode.previousElementSibling.getAttribute("data-field");
    var fieldElem = document.querySelector(`.${fieldElemeClass}`);

    var fieldElemeClassSection = fieldElemeClass.replace("section", "header");
    var fieldElemSection = document.querySelector(`.${fieldElemeClassSection}.info-top`);
    console.log(fieldElemSection);
    popUp.classList.add("hide");
    fieldElem.querySelector("span").textContent = lastFieldText[0];
    (fieldElemSection !== null && (
        fieldElemSection.querySelector("span").textContent = lastFieldText[0]
    ))
    
    lastFieldText = [];
    inputEditField.value = '';
}

/************************
SAVE - DESKTOP
************************/
function saveAction() {
    var fieldElemeClass = saveBtn.parentNode.previousElementSibling.getAttribute("data-field").replace("section", "header");
    var fieldElem;
    console.log(document.querySelector(`.${fieldElemeClass}.info-top span`) !== null);
    (document.querySelector(`.${fieldElemeClass}.info-top span`) !== null && (
        fieldElem = document.querySelector(`.${fieldElemeClass}.info-top span`),
        fieldElem.textContent = inputEditField.value
    ))
    var fieldElemeClassSection = saveBtn.parentNode.previousElementSibling.getAttribute("data-field");
    var fieldElemSection = document.querySelector(`.${fieldElemeClassSection} span`);
    console.log('fieldElemeClassSection', fieldElemSection);
    lastFieldText.unshift(fieldElemSection.textContent);
    closePopUp();
}

/***************************************
UPDATE INPUT (binding effect) - DESKTOP
***************************************/
function updateInputField() {
    var fieldElemeClass = this.getAttribute("data-field");
    var fieldElem = document.querySelector(`.${fieldElemeClass}`);

    var fieldElemeClassSection = this.getAttribute("data-field").replace("section", "header");
    var fieldElemSection = document.querySelector(`.${fieldElemeClassSection}.info-top`);
    console.log(fieldElemSection);
    
    var inputValue = this.value;
    lastFieldText.push(fieldElem.querySelector("span").textContent);
    fieldElem.querySelector("span").textContent = inputValue;
    (fieldElemSection !== null && (
        fieldElemSection.querySelector("span").textContent = inputValue
    ))
}

for(var i =0; i<testDiv.length; i++) {
	testDiv[i].addEventListener("click", showPopUp);
}

/************************
EVEN LISTENERS - DESKTOP
************************/
saveBtn.addEventListener("click", saveAction);
cancelBtn.addEventListener("click", closePopUp);
inputEditField.addEventListener("keyup", updateInputField);
Object.keys(headersOptions).map((e)=>{
    headersOptions[e].addEventListener("click", switchTabs)
})


/************************
MOBILE
************************/
const mobileForm = document.querySelector(".mobile-form")
const mobileEditBtn = document.querySelector(".mobile-edit")
const mobileFormButtons = document.querySelector(".mobile-form__pop-up-buttons")
const mobileCancelBtn = mobileFormButtons.querySelector(".cancel")
const mobileSaveBtn = mobileFormButtons.querySelector(".save")
const mobileInput = document.querySelectorAll(".mobile-input")
const name = document.getElementById("name")
const lastName = document.getElementById("last-name")
const sectionUserNameSplit = document.querySelector(".section__user-name span").textContent.split(' ')
const sectionUserName = document.querySelector(".section__user-name span")
const headerUserName = document.querySelector(".header__user-name span")
const headerUserChildren = document.querySelector(".header__user-info").children
const mobileFormItems = document.querySelectorAll(".mobile-form ul li")
const sectioninnerUserInfo = document.querySelectorAll(".section__inner-user-info > div")
let lastFieldTextMobile = []
let finalContent = []
let nameFormated

/************************
SAVE - MOBILE
************************/
function saveInfo() {
    Object.keys(mobileFormItems).map((e)=>{
       finalContent.push(mobileFormItems[e].querySelector('input').value)
    })
    
    nameFormated = `${finalContent[0]} ${finalContent[1]}`
    finalContent.shift()
    finalContent.shift()
    finalContent = [nameFormated, ...finalContent]
    console.log(finalContent)

    Object.keys(sectioninnerUserInfo).map((e,i)=>{
        sectioninnerUserInfo[e].querySelector('span').textContent = finalContent[i]
    })

    mobileForm.classList.add("hide")
    mobileEditBtn.classList.remove("hide")

    lastFieldTextMobile = []
   
}

/************************
SHOW - MOBILE
************************/
function showMobileForm() {
    mobileForm.classList.remove("hide")
    this.classList.add("hide")
    
    name.value = sectionUserNameSplit[0]
    lastName.value = sectionUserNameSplit[1]

    lastFieldText.push(headerUserName.textContent)

    Object.keys(headerUserChildren).map((elem) => {
        lastFieldTextMobile.push(headerUserChildren[elem].textContent)
    })

    console.log(lastFieldTextMobile)
    
}

/************************
CANCEL - MOBILE
************************/
function hideMobileForm() {
    mobileForm.classList.add("hide")
    mobileEditBtn.classList.remove("hide")

    headerUserName.textContent = lastFieldText[0]
    sectionUserName.textContent = lastFieldText[0]

    Object.keys(headerUserChildren).map((elem) => {
        headerUserChildren[elem].querySelector('span').textContent = lastFieldTextMobile[elem]
    })

    lastFieldTextMobile = []

}

/*************************************
UPDATE INPUT (binding effect)- MOBILE
**************************************/
function updateInputMobile(field, e) {
    if(field === 'name'){
        headerUserName.textContent = `${e.target.value} ${headerUserName.textContent.split(' ')[1]}`
        sectionUserName.textContent = `${e.target.value} ${headerUserName.textContent.split(' ')[1]}`
    }else if(field === 'lastName'){
        headerUserName.textContent = `${headerUserName.textContent.split(' ')[0]} ${e.target.value}`
        sectionUserName.textContent = `${headerUserName.textContent.split(' ')[0]} ${e.target.value}`
    }else if(field === 'address') {
        document.querySelector(`.header__${field} span`).textContent = e.target.value
    }else if(field === 'phone') {
        document.querySelector(`.header__${field} span`).textContent = e.target.value
    }
}

/************************
EVEN LISTENERS - DESKTOP
************************/
name.addEventListener("keyup", (e) => {updateInputMobile('name',e)})
lastName.addEventListener("keyup", (e) => {updateInputMobile('lastName',e)})
mobileEditBtn.addEventListener("click", showMobileForm)
mobileCancelBtn.addEventListener("click", hideMobileForm)
mobileSaveBtn.addEventListener("click", saveInfo)
Object.keys(mobileInput).map((elem) => {
    mobileInput[elem].addEventListener("keyup", (e) => {updateInputMobile(`${mobileInput[elem].getAttribute('id')}`, e)})
    mobileInput[elem].value = document.querySelector(`.${mobileInput[elem].getAttribute('data-field')} span`).textContent
})

let windowWidth = window.innerWidth
const extraMenuUl = document.querySelector('.extra-menu__options-ul')
const buttonDots = document.querySelector('.button__dots')
const menuItemsLength = headersOptions.length
const menuArr = []

buttonDots.addEventListener('click', ()=>{
    extraMenuUl.classList.toggle('hide')
})

function extraMenuFn(winWidth) {
    (winWidth > 1140 && (
        headersOptionsUl[0].append(headersOptions[menuItemsLength - 1])
    )) 
    if(winWidth <= 1140 && winWidth > 1070) {
        extraMenuUl.prepend(headersOptions[menuItemsLength - 1])
        headersOptionsUl[0].append(headersOptions[menuItemsLength - 2])
        if(menuArr.indexOf(headersOptions[menuItemsLength - 1]) === -1) {
            menuArr.unshift(headersOptions[menuItemsLength - 1])
        }
    }else if(winWidth <= 1070 && winWidth > 966) {
        extraMenuUl.prepend(headersOptions[menuItemsLength - 2])
        headersOptionsUl[0].append(headersOptions[menuItemsLength - 3])
        if(menuArr.indexOf(headersOptions[menuItemsLength - 2]) === -1) {
            menuArr.unshift(headersOptions[menuItemsLength - 2])
        }
    }else if(winWidth <= 966 && winWidth > 860) {
        extraMenuUl.prepend(headersOptions[menuItemsLength - 3])
        headersOptionsUl[0].append(headersOptions[menuItemsLength - 4])
        if(menuArr.indexOf(headersOptions[menuItemsLength - 3]) === -1) {
            menuArr.unshift(headersOptions[menuItemsLength - 3])
        }
    }else if(winWidth <= 860 && winWidth > 800) {
        extraMenuUl.prepend(headersOptions[menuItemsLength - 4])
        headersOptionsUl[0].append(headersOptions[menuItemsLength - 5])
        if(menuArr.indexOf(headersOptions[menuItemsLength - 4]) === -1) {
            menuArr.unshift(headersOptions[menuItemsLength - 4])
        }
    }


    if(winWidth <= 800 && winWidth > 780) {
        headersOptionsUl[0].append(...menuArr)
    } else if (winWidth <= 780 && winWidth > 712) {
        extraMenuUl.prepend(headersOptions[menuItemsLength - 1])
        headersOptionsUl[0].append(headersOptions[menuItemsLength - 2])

    } else if (winWidth <= 712 && winWidth > 680 ) {
        extraMenuUl.prepend(headersOptions[menuItemsLength - 2])
        headersOptionsUl[0].append(headersOptions[menuItemsLength - 3])

    } else if (winWidth <= 680 && winWidth > 520 ) {
        extraMenuUl.prepend(headersOptions[menuItemsLength - 3])
        headersOptionsUl[0].append(headersOptions[menuItemsLength - 4])
    
    } else if (winWidth <= 520 && winWidth > 400 ) {
        extraMenuUl.prepend(headersOptions[menuItemsLength - 4])
        headersOptionsUl[0].append(headersOptions[menuItemsLength - 5])

    } else if (winWidth <= 400 ) {
        extraMenuUl.prepend(headersOptions[menuItemsLength - 5])
    }
    
    

}

extraMenuFn(windowWidth)

window.onresize = () => {
    windowWidth = window.innerWidth
    extraMenuFn(windowWidth)
}