var gulp = require('gulp');
var sass = require('gulp-sass');
var cleanCSS = require('gulp-clean-css');
var rename = require('gulp-rename');

gulp.task('sass', function() {
    return gulp.src('sass/custom.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('assets/css/'));
});

gulp.task('min', ['sass'], () => {
    return gulp.src('assets/css/custom.css')
      .pipe(cleanCSS())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('assets'));
  });

gulp.task('watch', function () {
    gulp.watch("sass/**/*.scss", ['min']);
});

gulp.task('compile', ['sass', 'watch', 'min']);
